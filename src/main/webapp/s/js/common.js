/**
 * ajax提交表单
 * @param form_ 表单jquery对象
 * @param sf 成功请求后的方法
 * @param ef 异常请求后的方法
 */
var ajaxForm=function(form_,sf,ef){
    //验证成功后执行
    $(form_).on('valid.form', function(e, f){
        $.ajax({url:form_.attr("action"),data:form_.serialize(),dataType:'json',type:'POST',success:sf,error:ef?ef:function(e){
            layer.alert("访问异常!错误码:"+ e.status+"\t"+ e.statusText,{icon:5,time:2000,title:'系统提示',shadeClose:true});
        }});
    });
};
var ajaxLoadPage=function(obj,url_,data_){
    $.ajax({
        url:url_,
        type: "POST",
        data: data_,
        dataType: "html",
        success: function(html_) {
            $(obj).empty().html(html_);
        },error:function(e){
            layer.alert("加载异常!",{title:'系统提示',time:2000,icon:5,shadeClose:true})
        }
    })
}

var RESULT_SUCCESS="111";
var RESULT_ERROR="000";
var RESULT_WARNNING="222";
var RESULT_LOGIN_TIMEOUT="001";