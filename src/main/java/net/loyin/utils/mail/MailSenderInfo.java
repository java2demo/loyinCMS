package net.loyin.utils.mail;

/** 
 * 发送邮件需要使用的基本信息 
 */
import com.jfinal.core.Const;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import net.loyin.app.Constant;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.Properties;

public class MailSenderInfo {
	/** 接收邮件的服务器的URL 和端口 */
	public static String popServer;
	public static Integer popPort;
	/** 发送邮件的服务器的URL 和端口 */
	public static String smtpServer;
	public static Integer smtpPort;
	public static boolean ssl=true;
	/** 登陆邮件发送服务器的用户名 */
	public static String userName;
	/** 登陆邮件发送服务器的密码 */
	public static String password;
	/** 是否需要身份验证 */
	public static boolean isAuth = true;
	/** 邮件发送者的地址 */
	public String fromAddress;
	/** 邮件接收者的地址 */
	public String[] toAddress;
	/** 抄送人地址 */
	public String[] ccAddress;
	/** 密送人地址 */
	public String[] bccAddress;

	/** 邮件主题 */
	public String subject;
	/** 邮件的文本内容 */
	public String content;
	/** 邮件附件的文件名 */
	public String[] attachFileNames;

	public MailSenderInfo(){
		init();
	}
	/**
	 * 从Mongodb中查询邮件配置
	 */
	public void init(){
		Prop systemPro= PropKit.use(Constant.SYSTEM);
		popServer=systemPro.get("email_pop");
		popPort=Integer.parseInt(systemPro.get("email_pop_port"));
		smtpServer=systemPro.get("email_smtp");
		smtpPort=Integer.parseInt(systemPro.get("email_smtp_port"));
		fromAddress=systemPro.get("email_acount");
		userName=fromAddress;
		password=systemPro.get("email_acount_password");
	}
	/** 获得邮件会话属性 */
	public Properties getProperties() {
		Properties p = new Properties();
		p.put("mail.pop.host", this.popServer);
		p.put("mail.pop.port", this.popPort);
		p.put("mail.smtp.host", this.smtpServer);
		p.put("mail.smtp.port", this.smtpPort);
		p.put("mail.smtp.auth", isAuth ? "true" : "false");
		p.put("mail.pop.ssl.enable", ssl? "true" : "false");
		p.put("mail.smtp.ssl.enable", "true");
		return p;
	}
	/** 发送邮件 */
	public void sendMail() {
		new ThreadSend(this).start();
	}
	private class ThreadSend extends  Thread{
		Logger logger= Logger.getLogger(this.getClass());
		public MailSenderInfo info;
		public ThreadSend(MailSenderInfo info_){
			this.info=info_;
		}
		public void run(){
			try {
				SimpleMailSender sms = new SimpleMailSender();
				sms.sendHtmlMail(info);
			}catch (Exception e){
				logger.error("发送邮件异常",e);
			}
		}
	}
	public String[] getAttachFileNames() {
		return attachFileNames;
	}

	public void setAttachFileNames(String[] attachFileNames) {
		this.attachFileNames = attachFileNames;
	}

	public String[] getBccAddress() {
		return bccAddress;
	}

	public void setBccAddress(String[] bccAddress) {
		this.bccAddress = bccAddress;
	}

	public String[] getCcAddress() {
		return ccAddress;
	}

	public void setCcAddress(String[] ccAddress) {
		this.ccAddress = ccAddress;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public static String getPassword() {
		return password;
	}

	public static void setPassword(String password) {
		MailSenderInfo.password = password;
	}

	public static Integer getPopPort() {
		return popPort;
	}

	public static void setPopPort(Integer popPort) {
		MailSenderInfo.popPort = popPort;
	}

	public static String getPopServer() {
		return popServer;
	}

	public static void setPopServer(String popServer) {
		MailSenderInfo.popServer = popServer;
	}

	public static Integer getSmtpPort() {
		return smtpPort;
	}

	public static void setSmtpPort(Integer smtpPort) {
		MailSenderInfo.smtpPort = smtpPort;
	}

	public static String getSmtpServer() {
		return smtpServer;
	}

	public static void setSmtpServer(String smtpServer) {
		MailSenderInfo.smtpServer = smtpServer;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String[] getToAddress() {
		return toAddress;
	}

	public void setToAddress(String[] toAddress) {
		this.toAddress = toAddress;
	}

	public static String getUserName() {
		return userName;
	}

	public static void setUserName(String userName) {
		MailSenderInfo.userName = userName;
	}

	public static boolean isAuth() {
		return isAuth;
	}

	public static void setIsAuth(boolean isAuth) {
		MailSenderInfo.isAuth = isAuth;
	}
}