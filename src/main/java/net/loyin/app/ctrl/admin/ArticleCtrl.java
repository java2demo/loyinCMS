package net.loyin.app.ctrl.admin;

import net.loyin.annotation.ControllerBind;
import net.loyin.annotation.CtrlMethod;
import net.loyin.app.model.Article;
import net.loyin.app.model.ArticleCate;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 用户
 * Created by loyin on 16/1/12.
 */
@ControllerBind(route = "/admin/article")
public class ArticleCtrl extends AdminBaseCtrl {

    public void index(){
        String kwd=this.getPara("kwd");
        int pageNumber=this.getParaToInt("pageNumber",1);
        int pageSize=this.getParaToInt("pageSize",20);
        List<Object> paras=new ArrayList<Object>();
        StringBuffer sql=new StringBuffer(" from ");
        sql.append(Article.tableName);
        sql.append(" u,");
        sql.append(ArticleCate.tableName);
        sql.append(" c where c.id=u.cate_id ");
        if(StringUtils.isNotBlank(kwd)){
            sql.append(" and (u.title like ? or u.sup_title like ?)");
            kwd="%"+kwd+"%";
            paras.add(kwd);
            paras.add(kwd);
        }
        Integer status=this.getParaToInt("status");
        if(status!=null){
            sql.append(" and u.status=? ");
            paras.add(status);
        }
        String cate_id=this.getPara("cate_id");
        if(StringUtils.isNotBlank(cate_id)){
            sql.append(" and u.cate_id=? ");
            paras.add(cate_id);
        }
        sql.append(" order by u.update_datetime desc");
        this.keepPara();
        setCateList();
        this.setAttr("page",Article.dao.paginate(pageNumber,pageSize,"select u.*,c.name cate_name ",sql.toString(),paras.toArray()));
    }
    private void setCateList(){
        this.setAttr("cateList",ArticleCate.dao.find("select * from article_cate where status=1 order by sort asc"));
    }
    public void add(){
        String id=this.getPara(0);
        if(StringUtils.isNotBlank(id)){
            this.setAttr("po",Article.dao.findById(id));
        }
        setCateList();
    }
    @CtrlMethod(isAjax = true)
    public void save(){
        Article entity=this.getModel(Article.class);
        if(entity!=null){
            String id=entity.getId();
            if(StringUtils.isEmpty(id)) {
                entity.setCreateDatetime(new Date());
                entity.save();
            }else{
                entity.update();
            }
            id=entity.getId();
            this.renderJsonMsg(JSON_RESULT_SUCCESS,"保存成功!");
        }else{
            this.renderJsonMsg(JSON_RESULT_WARNNING,"参数错误!");
        }
    }
    @CtrlMethod(isAjax = true)
    public void del(){
        String id=this.getPara(0);
        try {
            if (StringUtils.isNotBlank(id)) {
                Article.dao.deleteById(id);
                this.renderJsonMsg(JSON_RESULT_SUCCESS, "删除成功!");
            } else {
                this.renderJsonMsg(JSON_RESULT_WARNNING, "缺少参数!");
            }
        }catch (Exception e){
            logger.error("删除异常",e);
            this.renderJsonMsg(JSON_RESULT_ERROR, "删除异常,可能存在引用,请先删除对应的数据!");
        }
    }
}
