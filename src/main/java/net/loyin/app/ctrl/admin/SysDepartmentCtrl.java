package net.loyin.app.ctrl.admin;

import net.loyin.annotation.ControllerBind;
import net.loyin.annotation.CtrlMethod;
import net.loyin.app.model.SysMenu;
import org.apache.commons.lang.StringUtils;

/**
 * Created by loyin on 16/1/14.
 */
@ControllerBind(route = "/admin/department")
public class SysDepartmentCtrl extends AdminBaseCtrl{
    public void index(){
        this.setAttr("dataList", SysMenu.dao.find("select * from sys_department order by sort asc"));
    }
    public void add(){
        String id=this.getPara(0);
        this.setAttr("parentid",this.getPara(1));
        this.setAttr("levelno",this.getPara(2));
        if(StringUtils.isNotBlank(id)){
            this.setAttr("po",SysMenu.dao.findById(id));
        }
    }
    @CtrlMethod(isAjax = true)
    public void save(){
        SysMenu entity=this.getModel(SysMenu.class);
        if(entity!=null){
            String id=entity.getId();
            if(StringUtils.isEmpty(id)) {
                entity.save();
            }else{
                entity.update();
            }
            this.renderJsonMsg(JSON_RESULT_SUCCESS,"保存成功!");
        }else{
            this.renderJsonMsg(JSON_RESULT_ERROR,"保存失败!");
        }
    }
    @CtrlMethod(isAjax = true)
    public void del(){
        String id=this.getPara(0);
        try {
            if (StringUtils.isNotBlank(id)) {
                SysMenu.dao.deleteById(id);
                this.renderJsonMsg(JSON_RESULT_SUCCESS, "删除成功!");
            } else {
                this.renderJsonMsg(JSON_RESULT_WARNNING, "缺少参数!");
            }
        }catch (Exception e){
            logger.error("删除异常",e);
            this.renderJsonMsg(JSON_RESULT_ERROR, "删除异常,可能存在子菜单,请先删除子菜单!");
        }
    }
}
