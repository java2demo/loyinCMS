package net.loyin.app.ctrl.front;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.plugin.ehcache.CacheInterceptor;
import com.jfinal.plugin.ehcache.CacheName;
import com.sun.org.apache.xml.internal.utils.StringBufferPool;
import net.loyin.annotation.ControllerBind;
import net.loyin.app.Constant;
import net.loyin.app.icp.PageCacheInterceptor;
import net.loyin.app.model.Article;
import net.loyin.app.model.ArticleCate;
import net.loyin.app.model.NavMenu;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 * 前端首页
 * Created by loyin on 16/1/12.
 */
@ControllerBind(route = "/")
public class FrontCtrl extends FrontBaseCtrl{
    /***
     * /{导航url}
     */
    public void index(){
        String para=this.getPara(0);
        if(StringUtils.isNotBlank(para)) {
            String jumpUrl = NavMenu.dao.hasJumpUrl("/"+ para, 1);
            if (StringUtils.isNotBlank(jumpUrl)) {
                this.forward(jumpUrl+(jumpUrl.indexOf("?")>-1?"&":"?")+"fromUrl=/"+ para);
            }
        }
    }
}
