package net.loyin.app.icp;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.plugin.ehcache.CacheKit;
import org.apache.commons.lang.StringUtils;

/**
 * 页面缓存拦截器 未完
 * Created by loyin on 16/1/19.
 */
public class PageCacheInterceptor implements Interceptor {
    @Override
    public void intercept(Invocation invocation) {
        Controller ctrl=invocation.getController();
        String uri=ctrl.getRequest().getRequestURI();
        String uri_c=CacheKit.get("page",uri);
        if(StringUtils.isEmpty(uri_c)){
            invocation.invoke();
        }
    }
}
