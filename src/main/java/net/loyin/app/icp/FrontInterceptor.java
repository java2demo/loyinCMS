package net.loyin.app.icp;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Const;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.render.*;
import net.loyin.annotation.CtrlMethod;
import net.loyin.app.Constant;
import net.loyin.app.ctrl.front.FrontBaseCtrl;
import net.loyin.app.model.Article;
import net.loyin.app.model.ArticleCate;
import net.loyin.app.model.Link;
import net.loyin.app.model.NavMenu;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * 前端拦截器
 * Created by loyin on 16/1/12.
 */
public class FrontInterceptor implements Interceptor {
    private static Logger logger = Logger.getLogger(FrontInterceptor.class);
    /**
     * 前端模板路径
     */
    public static String front_template;

    @Override
    public void intercept(Invocation invocation) {
        //获取系统配置
        Prop system_prop=PropKit.use(Constant.SYSTEM);
        if (front_template == null) {
            front_template = PropKit.get("jfinal_template_front_dir") + system_prop.get("jfinal_template_front");
        }
        FrontBaseCtrl ctrl = (FrontBaseCtrl) invocation.getController();
        String ctrKey = invocation.getControllerKey();
        String method = invocation.getMethodName();
        String view = PropKit.get("jfinal.base_view_path")+front_template + ctrKey + (ctrKey.endsWith("/") ? "" : "/") + method + ".html";

        //设置网站全局配置
        String web_domain=system_prop.get("web_front_domain");
        ctrl.setAttr("front_template",web_domain+"/s/"+front_template);
        ctrl.setAttr("web_domain",web_domain);
        ctrl.setAttr("web_keywords",system_prop.get("web_front_keywords"));
        ctrl.setAttr("web_description",system_prop.get("web_front_description"));
        ctrl.setAttr("web_front_beian",system_prop.get("web_front_beian"));
        ctrl.setAttr("website_tj",system_prop.get("website_tj"));
        ctrl.setAttr("website_footer",system_prop.get("website_footer"));

        //model设置
        ctrl.setAttr("ARTICLE", Article.dao);
        ctrl.setAttr("ARTICLE_CATE", ArticleCate.dao);
        ctrl.setAttr("linkList", Link.dao.qryAll());
        //顶部菜单导航
        ctrl.setAttr("navMenuTopList", NavMenu.dao.qryForCache(1));
        ctrl.setAttr("navMenuBottomList", NavMenu.dao.qryForCache(2));
        CtrlMethod ctrlMethod = (CtrlMethod) invocation.getMethod().getAnnotation(CtrlMethod.class);
        Date now=new Date();
        ctrl.setAttr("nowtime",now.getTime());
        ctrl.setAttr("now",now);
        boolean needRender = true;//是否需要重新render
        if (ctrlMethod != null) {
            //是否是ajax访问 用户登录超时提示 json
            if (ctrlMethod.isAjax()) {
                needRender = false;
            }
        }

        Render render = ctrl.getRender();
        if (needRender) {
            if (render == null) {//当CTRL的method里没有render时执行转换到对应的view
                ctrl.render(RenderFactory.me().getRender(view));
            } else {
                if ((render instanceof JsonRender) ||
                        (render.getClass().getSimpleName().endsWith("ActionRender")) ||
                        (render instanceof FileRender) ||
                        (render instanceof CaptchaRender) ||
                        (render instanceof ErrorRender) ||
                        (render instanceof FileRender) ||
                        (render instanceof IErrorRenderFactory) ||
                        (render instanceof IMainRenderFactory) ||
                        (render instanceof IXmlRenderFactory) ||
                        (render instanceof JavascriptRender) ||
                        (render instanceof JsonRender) ||
                        (render instanceof JspRender) ||
                        (render instanceof NullRender) ||
                        (render instanceof Redirect301Render) ||
                        (render instanceof RedirectRender) ||
                        (render instanceof TextRender) ||
                        (render instanceof XmlRender)) {

                } else {//视图渲染
//                    render.setView(front_template + ctrKey + (ctrKey.endsWith("/") ? "" : "/") + render.getView());
                    ctrl.render(RenderFactory.me().getRender(view));
                }
            }
        }
        invocation.invoke();
    }
}
