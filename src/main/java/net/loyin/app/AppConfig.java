package net.loyin.app;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallConfig;
import com.alibaba.druid.wall.WallFilter;
import com.alibaba.fastjson.JSON;
import com.jfinal.config.*;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.dialect.Dialect;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.dialect.OracleDialect;
import com.jfinal.plugin.activerecord.dialect.SqlServerDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import net.loyin.app.icp.AdminInterceptor;
import net.loyin.app.icp.FrontInterceptor;
import net.loyin.jfinal.dialect.MyPostgreSqlDialect;
import net.loyin.jfinal.ext.AutoBindRoutes;
import net.loyin.jfinal.ext.AutoTableBindPlugin;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

/**
 * Created by loyin on 15/6/20.
 */
public class AppConfig extends JFinalConfig {
    private Logger log= Logger.getLogger(getClass());
    /**数据源*/
    public static DruidPlugin druidPlugin;

    /**
     * 如果生产环境配置文件存在，则优先加载该配置，否则加载开发环境配置文件
     * @param pro 生产环境配置文件
     * @param dev 开发环境配置文件
     */
    public void loadProp(String pro, String dev) {
        try {
            PropKit.use(pro);
        }catch (Exception e) {
            PropKit.use(dev);
        }
    }

    public void configConstant(Constants me) {
        loadProp("config.txt", "config_dev.txt");
        me.setMaxPostSize(PropKit.getInt(Constant.FILE_UPLOAD_MAXSIZE, Constant.FILE_UPLOAD_MAXSIZE_DEF));
        me.setDevMode(PropKit.getBoolean(Constant.JFINAL_DEVMODE, Constant.JFINAL_DEVMODE_DEF));
        me.setBaseViewPath(PropKit.get("jfinal.base_view_path"));
        me.setI18nDefaultLocale("zh_CN");
//        me.setUploadedFileSaveDirectory(PropKit.get("file.upload.savedir"));
    }
    /**配置路由*/
    @Override
    public void configRoute(Routes routes) {
        AutoBindRoutes abr=new AutoBindRoutes();
        abr.suffix("Ctrl");
        abr.autoScan(true);
        routes.add(abr);
    }
    @Override
    public void configPlugin(Plugins me) {
        me.add(new EhCachePlugin());
        //加载可前端配置项文件
        Prop prop=PropKit.use(Constant.SYSTEM);
        //TODO 配置系统自用数据库
        String dbtype=prop.get(Constant.DB_TYPE);
        // 配置数据库连接池插件
        druidPlugin = new DruidPlugin(prop.get(Constant.DB_URL),
                prop.get(Constant.DB_USERNAME),
                prop.get(Constant.DB_PASSWORD),
                prop.get(Constant.DB_DRIVER));
        WallFilter wall = new WallFilter();
        wall.setDbType(PropKit.get(Constant.DB_TYPE));
        WallConfig wc = new WallConfig();
//		wc.setMultiStatementAllow(true);//支持多语句执行，要限于查询类。较危险的配置
        wall.setConfig(wc);
        druidPlugin.addFilter(wall);
        druidPlugin.addFilter(new StatFilter());
        druidPlugin.start();
        Dialect dialect=null;
        if("mysql".equals(dbtype)) {
            dialect=new MysqlDialect();
        }else if("oracle".equals(dbtype)) {
            dialect=new OracleDialect();
        }else if("sqlserver".equals(dbtype)) {
            dialect=new SqlServerDialect();
        }else if("postgresql".equals(dbtype)) {
            dialect=new MyPostgreSqlDialect();
        }

        //自动对应model
        AutoTableBindPlugin atbp = new AutoTableBindPlugin(druidPlugin);
        atbp.addScanPackages(PropKit.get("jfinal.scanPackages"));
        atbp.setShowSql(true);
        atbp.setDialect(dialect);// 配置数据库方言
        me.add(atbp);
    }

    @Override
    public void configInterceptor(Interceptors interceptors) {
        interceptors.add(new AdminInterceptor());
        interceptors.add(new FrontInterceptor());
    }

    @Override
    public void configHandler(Handlers me) {
        ContextPathHandler path = new ContextPathHandler("root");
        me.add(path);
    }

}
