package net.loyin.app.model;

import net.loyin.annotation.TableBind;
import net.loyin.app.model.base.BaseSysOplog;

/**
 * 操作日志
 */
@SuppressWarnings("serial")
@TableBind(tableName = SysOplog.tableName)
public class SysOplog extends BaseSysOplog<SysOplog> {
	public static final String tableName="sys_oplog";
	public static final SysOplog dao = new SysOplog();
}
