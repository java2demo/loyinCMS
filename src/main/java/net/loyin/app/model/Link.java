package net.loyin.app.model;

import net.loyin.annotation.TableBind;
import net.loyin.app.Constant;
import net.loyin.app.model.base.BaseLink;

import java.util.List;

/**
 * 友情链接
 */
@SuppressWarnings("serial")
@TableBind(tableName = Link.tableName)
public class Link extends BaseLink<Link> {
	public static final String tableName="link";
	public static final Link dao = new Link();
	public List<Link> qryAll(){
		return this.findByCache(Constant.CACHE_KEY_1DAY,tableName,"select * from link order by sort asc");
	}
}
