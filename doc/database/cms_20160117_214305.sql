--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: album; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE album (
    id character varying(50) NOT NULL,
    cate_id character varying(50) NOT NULL,
    title character varying(50) NOT NULL,
    sup_title character varying(100),
    summary text,
    content text,
    img character varying(100),
    img_list text,
    create_datetime timestamp without time zone DEFAULT now() NOT NULL,
    status smallint DEFAULT 0 NOT NULL,
    read_cout bigint DEFAULT 0 NOT NULL
);


ALTER TABLE album OWNER TO postgres;

--
-- Name: TABLE album; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE album IS '相册';


--
-- Name: COLUMN album.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album.id IS 'id';


--
-- Name: COLUMN album.cate_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album.cate_id IS 'cate_id';


--
-- Name: COLUMN album.title; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album.title IS '标题';


--
-- Name: COLUMN album.sup_title; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album.sup_title IS '子标题';


--
-- Name: COLUMN album.summary; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album.summary IS '摘要';


--
-- Name: COLUMN album.content; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album.content IS '内容';


--
-- Name: COLUMN album.img; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album.img IS '图片';


--
-- Name: COLUMN album.img_list; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album.img_list IS '图片列表';


--
-- Name: COLUMN album.create_datetime; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album.create_datetime IS 'create_datetime';


--
-- Name: COLUMN album.status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album.status IS 'status';


--
-- Name: COLUMN album.read_cout; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album.read_cout IS '阅览量';


--
-- Name: album_cate; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE album_cate (
    id character varying(50) NOT NULL,
    name character varying(50) NOT NULL,
    en_name character varying(50),
    summary text,
    sort smallint DEFAULT 0 NOT NULL,
    parentid character varying(50),
    levelno smallint DEFAULT 1 NOT NULL,
    status smallint DEFAULT 1 NOT NULL,
    code character varying(2044) COLLATE pg_catalog."POSIX" NOT NULL
);


ALTER TABLE album_cate OWNER TO postgres;

--
-- Name: TABLE album_cate; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE album_cate IS '相册类别';


--
-- Name: COLUMN album_cate.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album_cate.id IS 'id';


--
-- Name: COLUMN album_cate.name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album_cate.name IS 'name';


--
-- Name: COLUMN album_cate.en_name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album_cate.en_name IS '英文名称';


--
-- Name: COLUMN album_cate.summary; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album_cate.summary IS '摘要';


--
-- Name: COLUMN album_cate.sort; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album_cate.sort IS 'sort : 排序号';


--
-- Name: COLUMN album_cate.parentid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album_cate.parentid IS 'parentid';


--
-- Name: COLUMN album_cate.levelno; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album_cate.levelno IS 'levelno : 级别';


--
-- Name: COLUMN album_cate.status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN album_cate.status IS 'status';


--
-- Name: article; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE article (
    id character varying(50) NOT NULL,
    cate_id character varying(50) NOT NULL,
    title character varying(50) NOT NULL,
    sup_title character varying(100),
    summary text,
    content text NOT NULL,
    img character varying(100),
    create_datetime timestamp without time zone NOT NULL,
    status smallint DEFAULT 0 NOT NULL,
    read_cout bigint DEFAULT 0 NOT NULL
);


ALTER TABLE article OWNER TO postgres;

--
-- Name: TABLE article; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE article IS '文章';


--
-- Name: COLUMN article.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN article.id IS 'id';


--
-- Name: COLUMN article.cate_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN article.cate_id IS '分类id';


--
-- Name: COLUMN article.title; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN article.title IS '标题';


--
-- Name: COLUMN article.sup_title; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN article.sup_title IS '子标题';


--
-- Name: COLUMN article.summary; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN article.summary IS '摘要';


--
-- Name: COLUMN article.content; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN article.content IS '内容';


--
-- Name: COLUMN article.img; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN article.img IS '图片';


--
-- Name: COLUMN article.create_datetime; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN article.create_datetime IS 'create_datetime';


--
-- Name: COLUMN article.status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN article.status IS 'status';


--
-- Name: COLUMN article.read_cout; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN article.read_cout IS '阅览量';


--
-- Name: article_cate; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE article_cate (
    id character varying(50) NOT NULL,
    name character varying(50) NOT NULL,
    sort smallint DEFAULT 0 NOT NULL,
    parentid character varying(50),
    levelno smallint DEFAULT 1 NOT NULL,
    status smallint DEFAULT 1 NOT NULL,
    en_name character varying(50),
    code character varying(2044) COLLATE pg_catalog."POSIX" NOT NULL
);


ALTER TABLE article_cate OWNER TO postgres;

--
-- Name: TABLE article_cate; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE article_cate IS '文章类别';


--
-- Name: COLUMN article_cate.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN article_cate.id IS 'id';


--
-- Name: COLUMN article_cate.name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN article_cate.name IS 'name';


--
-- Name: COLUMN article_cate.sort; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN article_cate.sort IS 'sort : 排序号';


--
-- Name: COLUMN article_cate.parentid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN article_cate.parentid IS 'parentid';


--
-- Name: COLUMN article_cate.levelno; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN article_cate.levelno IS 'levelno : 级别';


--
-- Name: COLUMN article_cate.status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN article_cate.status IS 'status';


--
-- Name: COLUMN article_cate.en_name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN article_cate.en_name IS '英文名称';


--
-- Name: id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE id_seq
    START WITH 9999
    INCREMENT BY 1
    MINVALUE 9999
    MAXVALUE 999999999999999999
    CACHE 100;


ALTER TABLE id_seq OWNER TO postgres;

--
-- Name: nav_menu; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE nav_menu (
    id character varying(50) NOT NULL,
    name character varying(50) NOT NULL,
    en_name character varying(50),
    sort smallint DEFAULT 0 NOT NULL,
    parentid character varying(50),
    levelno smallint DEFAULT 1 NOT NULL,
    status smallint DEFAULT 1 NOT NULL,
    type smallint DEFAULT 0 NOT NULL,
    jump_url character varying(100),
    url character varying(100) NOT NULL
);


ALTER TABLE nav_menu OWNER TO postgres;

--
-- Name: TABLE nav_menu; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE nav_menu IS '导航菜单';


--
-- Name: COLUMN nav_menu.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN nav_menu.id IS 'id';


--
-- Name: COLUMN nav_menu.name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN nav_menu.name IS 'name';


--
-- Name: COLUMN nav_menu.en_name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN nav_menu.en_name IS '英文名称';


--
-- Name: COLUMN nav_menu.sort; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN nav_menu.sort IS 'sort : 排序号';


--
-- Name: COLUMN nav_menu.parentid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN nav_menu.parentid IS 'parentid';


--
-- Name: COLUMN nav_menu.levelno; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN nav_menu.levelno IS 'levelno : 级别';


--
-- Name: COLUMN nav_menu.status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN nav_menu.status IS 'status';


--
-- Name: COLUMN nav_menu.type; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN nav_menu.type IS 'type';


--
-- Name: COLUMN nav_menu.jump_url; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN nav_menu.jump_url IS 'jump_url : 实际跳转的url';


--
-- Name: COLUMN nav_menu.url; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN nav_menu.url IS 'url';


--
-- Name: sys_department; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sys_department (
    id character varying(50) NOT NULL,
    type smallint NOT NULL,
    name character varying(50) NOT NULL,
    en_name character varying(50),
    director character varying(50),
    summary text,
    sort smallint DEFAULT 0 NOT NULL,
    parentid character varying(50),
    levelno smallint DEFAULT 1 NOT NULL,
    status smallint DEFAULT 1 NOT NULL
);


ALTER TABLE sys_department OWNER TO postgres;

--
-- Name: TABLE sys_department; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE sys_department IS '部门';


--
-- Name: COLUMN sys_department.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_department.id IS 'id';


--
-- Name: COLUMN sys_department.type; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_department.type IS 'type';


--
-- Name: COLUMN sys_department.name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_department.name IS 'name';


--
-- Name: COLUMN sys_department.en_name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_department.en_name IS '英文名称';


--
-- Name: COLUMN sys_department.director; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_department.director IS '负责人';


--
-- Name: COLUMN sys_department.summary; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_department.summary IS '摘要';


--
-- Name: COLUMN sys_department.sort; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_department.sort IS 'sort : 排序号';


--
-- Name: COLUMN sys_department.parentid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_department.parentid IS 'parentid';


--
-- Name: COLUMN sys_department.levelno; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_department.levelno IS 'levelno : 级别';


--
-- Name: COLUMN sys_department.status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_department.status IS 'status';


--
-- Name: sys_dict; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sys_dict (
    id character varying(50) NOT NULL,
    dicttype smallint DEFAULT 0 NOT NULL,
    name character varying(50) NOT NULL,
    en_name character varying(50),
    code character varying(50) NOT NULL,
    remark text,
    status smallint DEFAULT 1 NOT NULL
);


ALTER TABLE sys_dict OWNER TO postgres;

--
-- Name: TABLE sys_dict; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE sys_dict IS '数据字典';


--
-- Name: COLUMN sys_dict.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_dict.id IS 'id';


--
-- Name: COLUMN sys_dict.dicttype; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_dict.dicttype IS 'dicttype : 0:单列
1:树形';


--
-- Name: COLUMN sys_dict.name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_dict.name IS 'name';


--
-- Name: COLUMN sys_dict.en_name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_dict.en_name IS '英文名称';


--
-- Name: COLUMN sys_dict.code; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_dict.code IS 'code';


--
-- Name: COLUMN sys_dict.remark; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_dict.remark IS 'remark';


--
-- Name: COLUMN sys_dict.status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_dict.status IS 'status';


--
-- Name: sys_dict_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sys_dict_detail (
    id character varying(50) NOT NULL,
    parentid character varying(50),
    dict_id character varying(50) NOT NULL,
    name character varying(50) NOT NULL,
    en_name character varying(50),
    code character varying(50),
    status smallint DEFAULT 1 NOT NULL,
    levelno smallint DEFAULT 1 NOT NULL
);


ALTER TABLE sys_dict_detail OWNER TO postgres;

--
-- Name: TABLE sys_dict_detail; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE sys_dict_detail IS '数据字典详细项';


--
-- Name: COLUMN sys_dict_detail.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_dict_detail.id IS 'id';


--
-- Name: COLUMN sys_dict_detail.parentid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_dict_detail.parentid IS 'parentid';


--
-- Name: COLUMN sys_dict_detail.dict_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_dict_detail.dict_id IS 'dict_id';


--
-- Name: COLUMN sys_dict_detail.name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_dict_detail.name IS 'name';


--
-- Name: COLUMN sys_dict_detail.en_name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_dict_detail.en_name IS '英文名称';


--
-- Name: COLUMN sys_dict_detail.code; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_dict_detail.code IS 'code';


--
-- Name: COLUMN sys_dict_detail.status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_dict_detail.status IS 'status';


--
-- Name: COLUMN sys_dict_detail.levelno; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_dict_detail.levelno IS 'levelno : 级别';


--
-- Name: sys_menu; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sys_menu (
    id character varying(50) NOT NULL,
    name character varying(50) NOT NULL,
    sort smallint DEFAULT 0 NOT NULL,
    parentid character varying(50),
    levelno smallint DEFAULT 1 NOT NULL,
    status smallint DEFAULT 1 NOT NULL,
    type smallint DEFAULT 0 NOT NULL,
    url character varying(100),
    urlkey character varying(50),
    icon character varying(50) COLLATE pg_catalog."POSIX"
);


ALTER TABLE sys_menu OWNER TO postgres;

--
-- Name: TABLE sys_menu; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE sys_menu IS '菜单';


--
-- Name: COLUMN sys_menu.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_menu.id IS 'id';


--
-- Name: COLUMN sys_menu.name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_menu.name IS 'name';


--
-- Name: COLUMN sys_menu.sort; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_menu.sort IS 'sort : 排序号';


--
-- Name: COLUMN sys_menu.parentid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_menu.parentid IS 'parentid';


--
-- Name: COLUMN sys_menu.levelno; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_menu.levelno IS 'levelno : 级别';


--
-- Name: COLUMN sys_menu.status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_menu.status IS 'status';


--
-- Name: COLUMN sys_menu.type; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_menu.type IS 'type';


--
-- Name: COLUMN sys_menu.url; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_menu.url IS 'url';


--
-- Name: COLUMN sys_menu.urlkey; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_menu.urlkey IS 'urlkey : 显示的url';


--
-- Name: sys_oplog; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sys_oplog (
    id character varying(50) NOT NULL,
    menu_name character varying(50) NOT NULL,
    opresult text,
    req_data text,
    url character varying(100) NOT NULL,
    user_id character varying(50) NOT NULL,
    ip character varying(50) NOT NULL,
    create_datetime timestamp without time zone NOT NULL
);


ALTER TABLE sys_oplog OWNER TO postgres;

--
-- Name: TABLE sys_oplog; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE sys_oplog IS '操作日志';


--
-- Name: COLUMN sys_oplog.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_oplog.id IS 'id';


--
-- Name: COLUMN sys_oplog.menu_name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_oplog.menu_name IS 'menu_name';


--
-- Name: COLUMN sys_oplog.opresult; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_oplog.opresult IS '操作结果';


--
-- Name: COLUMN sys_oplog.req_data; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_oplog.req_data IS '请求数据';


--
-- Name: COLUMN sys_oplog.url; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_oplog.url IS 'url';


--
-- Name: COLUMN sys_oplog.user_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_oplog.user_id IS 'user_id';


--
-- Name: COLUMN sys_oplog.ip; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_oplog.ip IS 'ip';


--
-- Name: COLUMN sys_oplog.create_datetime; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_oplog.create_datetime IS 'create_datetime';


--
-- Name: sys_role; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sys_role (
    id character varying(50) NOT NULL,
    name character varying(50) NOT NULL,
    remark text,
    status smallint DEFAULT 1 NOT NULL
);


ALTER TABLE sys_role OWNER TO postgres;

--
-- Name: TABLE sys_role; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE sys_role IS '角色';


--
-- Name: COLUMN sys_role.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_role.id IS 'id';


--
-- Name: COLUMN sys_role.name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_role.name IS 'name';


--
-- Name: COLUMN sys_role.remark; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_role.remark IS 'remark';


--
-- Name: COLUMN sys_role.status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_role.status IS 'status';


--
-- Name: sys_role_menu; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sys_role_menu (
    role_id character varying(50) NOT NULL,
    menu_id character varying(50) NOT NULL
);


ALTER TABLE sys_role_menu OWNER TO postgres;

--
-- Name: sys_user; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sys_user (
    id character varying(50) NOT NULL,
    account character varying(50) NOT NULL,
    password character varying(50) NOT NULL,
    status smallint DEFAULT 1 NOT NULL,
    realname character varying(20),
    sex smallint DEFAULT 0 NOT NULL,
    email character varying(50),
    mobile character varying(50),
    create_datetime timestamp without time zone DEFAULT now() NOT NULL,
    last_login_ip character varying(20),
    last_login_time timestamp without time zone,
    avator character varying(50) COLLATE pg_catalog."POSIX"
);


ALTER TABLE sys_user OWNER TO postgres;

--
-- Name: TABLE sys_user; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE sys_user IS '系统用户';


--
-- Name: COLUMN sys_user.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_user.id IS 'id';


--
-- Name: COLUMN sys_user.account; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_user.account IS 'account';


--
-- Name: COLUMN sys_user.password; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_user.password IS 'password';


--
-- Name: COLUMN sys_user.status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_user.status IS 'status';


--
-- Name: COLUMN sys_user.realname; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_user.realname IS 'realname';


--
-- Name: COLUMN sys_user.sex; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_user.sex IS 'sex : 0:未知
1:男
2:女';


--
-- Name: COLUMN sys_user.email; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_user.email IS 'email';


--
-- Name: COLUMN sys_user.mobile; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_user.mobile IS 'mobile';


--
-- Name: COLUMN sys_user.create_datetime; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_user.create_datetime IS 'create_datetime';


--
-- Name: COLUMN sys_user.last_login_ip; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_user.last_login_ip IS 'last_login_ip';


--
-- Name: COLUMN sys_user.last_login_time; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_user.last_login_time IS 'last_login_time';


--
-- Name: sys_user_role; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sys_user_role (
    user_id character varying(50) NOT NULL,
    role_id character varying(50) NOT NULL
);


ALTER TABLE sys_user_role OWNER TO postgres;

--
-- Name: TABLE sys_user_role; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE sys_user_role IS 'sys_user_role';


--
-- Name: COLUMN sys_user_role.user_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_user_role.user_id IS 'user_id';


--
-- Name: COLUMN sys_user_role.role_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sys_user_role.role_id IS 'role_id';


--
-- Data for Name: album; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO album (id, cate_id, title, sup_title, summary, content, img, img_list, create_datetime, status, read_cout) VALUES ('Opg', 'OLG', 'fdsaf', 'fdsfsad', '<p>fdasfsdaf</p>', '<p>fdsafdsafsdaf</p>', 'fdsafdsf', NULL, '2016-01-17 00:04:53.144', 1, 0);


--
-- Data for Name: album_cate; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO album_cate (id, name, en_name, summary, sort, parentid, levelno, status, code) VALUES ('OLG', '相册1', 'fdsa', '<p style="text-align: center;"><span style="font-size: 36px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;">范德萨方式地方<sup>s</sup>/<sub>d</sub></span></p><pre class="brush:java;toolbar:false">public&nbsp;class&nbsp;Tests{
}</pre><p style="text-align: center;"><span style="font-size: 36px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;"></span><br/></p><p style="text-align:center"></p><p style="text-align: center;"><img src="/upload/image/20160116/1452954042363038101.jpg" alt="1452954042363038101.jpg" width="500" height="624" border="0" vspace="0" title="1452954042363038101.jpg" style="width: 500px; height: 624px;"/></p><p style="text-align: center;"><br/></p>_ueditor_page_break_tag_<p><iframe src="http://127.0.0.1:8080/s/plugins/ueditor/dialogs/map/show.html#center=5.13954,-48.65556&zoom=2&width=530&height=340&markers=116.402996,39.911924&markerStyles=l,A" frameborder="0"></iframe></p><p><br/></p>', 1, NULL, 1, 1, '23');
INSERT INTO album_cate (id, name, en_name, summary, sort, parentid, levelno, status, code) VALUES ('O1D', 'dsf', 'sdf', '<p>fdsaf</p>', 1, 'OLG', 2, 1, 'sdf');


--
-- Data for Name: article; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO article (id, cate_id, title, sup_title, summary, content, img, create_datetime, status, read_cout) VALUES ('OGl', '1X8', 'fdsaf', 'fdsaf', '<p>fdsafsdafsd</p>', '<p>fdsaffdsafdfsa</p>', 'fdsafdsaf', '2016-01-16 23:27:00.116', 1, 0);


--
-- Data for Name: article_cate; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO article_cate (id, name, sort, parentid, levelno, status, en_name, code) VALUES ('1X8', '新闻', 1, NULL, 1, 1, 'news', 'news');
INSERT INTO article_cate (id, name, sort, parentid, levelno, status, en_name, code) VALUES ('1X5', '校内新闻', 1, '1X8', 2, 1, 'school news', 'shcoolnews');
INSERT INTO article_cate (id, name, sort, parentid, levelno, status, en_name, code) VALUES ('1Xz', '办公新闻', 1, '1X5', 3, 1, 'oanews', 'oanews');


--
-- Name: id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('id_seq', 12298, true);


--
-- Data for Name: nav_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO nav_menu (id, name, en_name, sort, parentid, levelno, status, type, jump_url, url) VALUES ('OVb', '新闻', NULL, 1, NULL, 1, 1, 1, '/article/1', '/news');
INSERT INTO nav_menu (id, name, en_name, sort, parentid, levelno, status, type, jump_url, url) VALUES ('OVf', '集团新闻', NULL, 1, 'OVb', 2, 1, 1, '/article/2', '/news/jt');


--
-- Data for Name: sys_department; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sys_dict; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sys_dict_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sys_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO sys_menu (id, name, sort, parentid, levelno, status, type, url, urlkey, icon) VALUES ('002', '菜单', 1, '001', 2, 1, 1, '/menu', 'menu', 'ti-menu-alt');
INSERT INTO sys_menu (id, name, sort, parentid, levelno, status, type, url, urlkey, icon) VALUES ('003', '用户', 2, '001', 2, 1, 1, '/sysUser', 'sysuser', 'ti-user');
INSERT INTO sys_menu (id, name, sort, parentid, levelno, status, type, url, urlkey, icon) VALUES ('006', '角色', 1, '001', 2, 1, 1, '/role', 'role', 'fa fa-users');
INSERT INTO sys_menu (id, name, sort, parentid, levelno, status, type, url, urlkey, icon) VALUES ('1MP', '添加', 1, '002', 3, 1, 2, '/menu/add', 'sysmenuadd', 'fa fa-plus');
INSERT INTO sys_menu (id, name, sort, parentid, levelno, status, type, url, urlkey, icon) VALUES ('1M7', '删除', 1, '002', 3, 1, 2, '/menu/del', 'sysmenudel', 'fa fa-trash');
INSERT INTO sys_menu (id, name, sort, parentid, levelno, status, type, url, urlkey, icon) VALUES ('1Mi', '编辑', 1, '002', 3, 1, 2, '/menu', 'sysmenuadd', 'fa fa-edit');
INSERT INTO sys_menu (id, name, sort, parentid, levelno, status, type, url, urlkey, icon) VALUES ('1EL', '内容管理', 2, NULL, 1, 1, 1, NULL, NULL, ' ti-pencil-alt');
INSERT INTO sys_menu (id, name, sort, parentid, levelno, status, type, url, urlkey, icon) VALUES ('1E1', '文章类别', 1, '1EL', 2, 1, 1, '/articleCate', 'articleCate', ' ti-bookmark-alt');
INSERT INTO sys_menu (id, name, sort, parentid, levelno, status, type, url, urlkey, icon) VALUES ('001', '系统', 1, NULL, 1, 1, 1, NULL, NULL, 'ti-settings');
INSERT INTO sys_menu (id, name, sort, parentid, levelno, status, type, url, urlkey, icon) VALUES ('1Xn', '文章', 2, '1EL', 2, 1, 1, '/article', 'article', 'ti-book');
INSERT INTO sys_menu (id, name, sort, parentid, levelno, status, type, url, urlkey, icon) VALUES ('1Xg', '相册类别', 1, '1Xj', 2, 1, 1, '/albumCate', 'albumCate', 'ti-image');
INSERT INTO sys_menu (id, name, sort, parentid, levelno, status, type, url, urlkey, icon) VALUES ('1XA', '相册列表', 2, '1Xj', 2, 1, 1, '/album', 'album', 'ti-image');
INSERT INTO sys_menu (id, name, sort, parentid, levelno, status, type, url, urlkey, icon) VALUES ('1Xj', '相册管理', 3, NULL, 1, 1, 1, NULL, NULL, 'ti-image');
INSERT INTO sys_menu (id, name, sort, parentid, levelno, status, type, url, urlkey, icon) VALUES ('Odd', '网站导航', 4, '001', 2, 1, 1, '/navMenu', '/navMenu', 'ti-map-alt');
INSERT INTO sys_menu (id, name, sort, parentid, levelno, status, type, url, urlkey, icon) VALUES ('005', '日志', 5, '001', 2, 1, 1, '/oplog', 'oplog', ' ti-target');
INSERT INTO sys_menu (id, name, sort, parentid, levelno, status, type, url, urlkey, icon) VALUES ('004', '字典', 3, '001', 2, 1, 1, '/dict', 'dict', 'ti-book');
INSERT INTO sys_menu (id, name, sort, parentid, levelno, status, type, url, urlkey, icon) VALUES ('OVB', '系统设置', 7, '001', 2, 1, 1, '/system', 'system', 'ti-settings');


--
-- Data for Name: sys_oplog; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sys_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO sys_role (id, name, remark, status) VALUES ('1tT', '员工', '发达省份', 1);
INSERT INTO sys_role (id, name, remark, status) VALUES ('001', '管理员', 'fdsa', 1);


--
-- Data for Name: sys_role_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO sys_role_menu (role_id, menu_id) VALUES ('001', '001');
INSERT INTO sys_role_menu (role_id, menu_id) VALUES ('001', 'OVB');
INSERT INTO sys_role_menu (role_id, menu_id) VALUES ('001', '006');
INSERT INTO sys_role_menu (role_id, menu_id) VALUES ('001', '002');
INSERT INTO sys_role_menu (role_id, menu_id) VALUES ('001', '1MP');
INSERT INTO sys_role_menu (role_id, menu_id) VALUES ('001', '1M7');
INSERT INTO sys_role_menu (role_id, menu_id) VALUES ('001', '1Mi');
INSERT INTO sys_role_menu (role_id, menu_id) VALUES ('001', '003');
INSERT INTO sys_role_menu (role_id, menu_id) VALUES ('001', 'Odd');
INSERT INTO sys_role_menu (role_id, menu_id) VALUES ('001', '1EL');
INSERT INTO sys_role_menu (role_id, menu_id) VALUES ('001', '1E1');
INSERT INTO sys_role_menu (role_id, menu_id) VALUES ('001', '1Xn');
INSERT INTO sys_role_menu (role_id, menu_id) VALUES ('001', '1Xj');
INSERT INTO sys_role_menu (role_id, menu_id) VALUES ('001', '1Xg');
INSERT INTO sys_role_menu (role_id, menu_id) VALUES ('001', '1XA');
INSERT INTO sys_role_menu (role_id, menu_id) VALUES ('1tT', '1EL');
INSERT INTO sys_role_menu (role_id, menu_id) VALUES ('1tT', '1E1');


--
-- Data for Name: sys_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO sys_user (id, account, password, status, realname, sex, email, mobile, create_datetime, last_login_ip, last_login_time, avator) VALUES ('OCK', 'fdsafasf', 'OtiCpOUXLug=', 1, 'fdsafsaf', 0, 'fdsaf@12.com', NULL, '2016-01-16 23:18:12.44412', NULL, NULL, NULL);
INSERT INTO sys_user (id, account, password, status, realname, sex, email, mobile, create_datetime, last_login_ip, last_login_time, avator) VALUES ('001', 'admin', 'IpWVwl7o6fs=', 1, '管理员', 0, 'loyin@loyin.net', NULL, '2016-01-13 00:00:00', '127.0.0.1', '2016-01-17 17:04:18.075', NULL);


--
-- Data for Name: sys_user_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO sys_user_role (user_id, role_id) VALUES ('OCK', '001');
INSERT INTO sys_user_role (user_id, role_id) VALUES ('001', '001');
INSERT INTO sys_user_role (user_id, role_id) VALUES ('001', '001');
INSERT INTO sys_user_role (user_id, role_id) VALUES ('001', '001');


--
-- Name: album_cate_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY album_cate
    ADD CONSTRAINT album_cate_code_key UNIQUE (code);


--
-- Name: album_cate_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY album_cate
    ADD CONSTRAINT album_cate_pkey PRIMARY KEY (id);


--
-- Name: album_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY album
    ADD CONSTRAINT album_pkey PRIMARY KEY (id);


--
-- Name: article_cate_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY article_cate
    ADD CONSTRAINT article_cate_code_key UNIQUE (code);


--
-- Name: article_cate_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY article_cate
    ADD CONSTRAINT article_cate_pkey PRIMARY KEY (id);


--
-- Name: article_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY article
    ADD CONSTRAINT article_pkey PRIMARY KEY (id);


--
-- Name: nav_menu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY nav_menu
    ADD CONSTRAINT nav_menu_pkey PRIMARY KEY (id);


--
-- Name: nav_menu_url_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY nav_menu
    ADD CONSTRAINT nav_menu_url_key UNIQUE (url);


--
-- Name: sys_department_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sys_department
    ADD CONSTRAINT sys_department_pkey PRIMARY KEY (id);


--
-- Name: sys_dict_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sys_dict_detail
    ADD CONSTRAINT sys_dict_detail_pkey PRIMARY KEY (id);


--
-- Name: sys_dict_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sys_dict
    ADD CONSTRAINT sys_dict_pkey PRIMARY KEY (id);


--
-- Name: sys_menu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sys_menu
    ADD CONSTRAINT sys_menu_pkey PRIMARY KEY (id);


--
-- Name: sys_oplog_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sys_oplog
    ADD CONSTRAINT sys_oplog_pkey PRIMARY KEY (id);


--
-- Name: sys_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sys_role
    ADD CONSTRAINT sys_role_pkey PRIMARY KEY (id);


--
-- Name: sys_user_account_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sys_user
    ADD CONSTRAINT sys_user_account_key UNIQUE (account);


--
-- Name: sys_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sys_user
    ADD CONSTRAINT sys_user_pkey PRIMARY KEY (id);


--
-- Name: article_cate_code_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX article_cate_code_idx ON article_cate USING btree (code);


--
-- Name: album_cate_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY album
    ADD CONSTRAINT album_cate_id_fkey FOREIGN KEY (cate_id) REFERENCES album_cate(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: album_cate_parentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY album_cate
    ADD CONSTRAINT album_cate_parentid_fkey FOREIGN KEY (parentid) REFERENCES album_cate(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: album_cate_parentid_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY album_cate
    ADD CONSTRAINT album_cate_parentid_fkey1 FOREIGN KEY (parentid) REFERENCES album_cate(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: album_cate_parentid_fkey2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY album_cate
    ADD CONSTRAINT album_cate_parentid_fkey2 FOREIGN KEY (parentid) REFERENCES album_cate(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: album_cate_parentid_fkey3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY album_cate
    ADD CONSTRAINT album_cate_parentid_fkey3 FOREIGN KEY (parentid) REFERENCES album_cate(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: article_cate_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY article
    ADD CONSTRAINT article_cate_id_fkey FOREIGN KEY (cate_id) REFERENCES article_cate(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: article_cate_parentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY article_cate
    ADD CONSTRAINT article_cate_parentid_fkey FOREIGN KEY (parentid) REFERENCES article_cate(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: article_cate_parentid_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY article_cate
    ADD CONSTRAINT article_cate_parentid_fkey1 FOREIGN KEY (parentid) REFERENCES article_cate(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: article_cate_parentid_fkey2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY article_cate
    ADD CONSTRAINT article_cate_parentid_fkey2 FOREIGN KEY (parentid) REFERENCES article_cate(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: article_cate_parentid_fkey3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY article_cate
    ADD CONSTRAINT article_cate_parentid_fkey3 FOREIGN KEY (parentid) REFERENCES article_cate(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: nav_menu_parentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY nav_menu
    ADD CONSTRAINT nav_menu_parentid_fkey FOREIGN KEY (parentid) REFERENCES nav_menu(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: nav_menu_parentid_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY nav_menu
    ADD CONSTRAINT nav_menu_parentid_fkey1 FOREIGN KEY (parentid) REFERENCES nav_menu(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: nav_menu_parentid_fkey2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY nav_menu
    ADD CONSTRAINT nav_menu_parentid_fkey2 FOREIGN KEY (parentid) REFERENCES nav_menu(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: nav_menu_parentid_fkey3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY nav_menu
    ADD CONSTRAINT nav_menu_parentid_fkey3 FOREIGN KEY (parentid) REFERENCES nav_menu(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: sys_dict_detail_dict_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sys_dict_detail
    ADD CONSTRAINT sys_dict_detail_dict_id_fkey FOREIGN KEY (dict_id) REFERENCES sys_dict(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: sys_dict_detail_parentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sys_dict_detail
    ADD CONSTRAINT sys_dict_detail_parentid_fkey FOREIGN KEY (parentid) REFERENCES sys_dict_detail(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: sys_dict_detail_parentid_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sys_dict_detail
    ADD CONSTRAINT sys_dict_detail_parentid_fkey1 FOREIGN KEY (parentid) REFERENCES sys_dict_detail(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: sys_dict_detail_parentid_fkey2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sys_dict_detail
    ADD CONSTRAINT sys_dict_detail_parentid_fkey2 FOREIGN KEY (parentid) REFERENCES sys_dict_detail(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: sys_dict_detail_parentid_fkey3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sys_dict_detail
    ADD CONSTRAINT sys_dict_detail_parentid_fkey3 FOREIGN KEY (parentid) REFERENCES sys_dict_detail(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: sys_menu_parentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sys_menu
    ADD CONSTRAINT sys_menu_parentid_fkey FOREIGN KEY (parentid) REFERENCES sys_menu(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: sys_menu_parentid_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sys_menu
    ADD CONSTRAINT sys_menu_parentid_fkey1 FOREIGN KEY (parentid) REFERENCES sys_menu(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: sys_menu_parentid_fkey2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sys_menu
    ADD CONSTRAINT sys_menu_parentid_fkey2 FOREIGN KEY (parentid) REFERENCES sys_menu(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: sys_menu_parentid_fkey3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sys_menu
    ADD CONSTRAINT sys_menu_parentid_fkey3 FOREIGN KEY (parentid) REFERENCES sys_menu(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: sys_oplog_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sys_oplog
    ADD CONSTRAINT sys_oplog_user_id_fkey FOREIGN KEY (user_id) REFERENCES sys_user(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: sys_role_menu_menu_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sys_role_menu
    ADD CONSTRAINT sys_role_menu_menu_id_fkey FOREIGN KEY (menu_id) REFERENCES sys_menu(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: sys_role_menu_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sys_role_menu
    ADD CONSTRAINT sys_role_menu_role_id_fkey FOREIGN KEY (role_id) REFERENCES sys_role(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: sys_user_role_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sys_user_role
    ADD CONSTRAINT sys_user_role_role_id_fkey FOREIGN KEY (role_id) REFERENCES sys_role(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: sys_user_role_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sys_user_role
    ADD CONSTRAINT sys_user_role_user_id_fkey FOREIGN KEY (user_id) REFERENCES sys_user(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

